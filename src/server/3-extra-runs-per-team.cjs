const fs = require('fs')

const papa = require('papaparse')

const csvFilePathDeliveries = 'src/data/deliveries.csv'

const csvFilePathMatches = 'src/data/matches.csv'


fs.readFile(csvFilePathDeliveries, 'utf-8', (err, data) => {

    if(err) {
        console.log('Error in reading the file.')
        return
    }
    const parsedData = papa.parse(data, {
        header : true
    })
    const deliveries = parsedData.data;

    fs.readFile(csvFilePathMatches, 'utf-8', (err, data) => {
        if(err) {
            console.log('Error in reading the file.')
            return
        }
        const parsedData = papa.parse(data, {
            header : true
        })

        const matches = parsedData.data

        const matchIdSet = (matches) => {
            let extraRuns = matches.reduce((accumulator, match) => {
                let season = match['season']
                if(season == '2016'){
                    accumulator.add(match['id'])
                }
                return accumulator
            }, new Set())
            return extraRuns
        }
        
        const getExtraRunsPerTeam = (deliveries) => {
            let idSet = matchIdSet(matches)
            let extraRuns = deliveries.reduce((accumulator, delivery) => {
                let battingTeam = delivery['batting_team']
                if(idSet.has(delivery['match_id'])){
                    if(accumulator[battingTeam] === undefined){
                        accumulator[battingTeam] = parseInt(delivery['extra_runs'])
                        return accumulator
                    }
                    else{
                        accumulator[battingTeam] += parseInt(delivery['extra_runs'])
                        return accumulator
                    }
                }
                return accumulator
            }, {})
            return extraRuns
        }

        console.log(getExtraRunsPerTeam(deliveries))

        let answer = getExtraRunsPerTeam(deliveries)

        const jsonData = JSON.stringify(answer);

        fs.writeFile('src/public/output/3-extra-runs-per-team.json', jsonData, 'utf8', (err) => {
            if (err) {
            console.error('Error writing JSON data to file:', err)
            return
            }
            console.log('CSV data converted to JSON and saved as output.json')
        })
    })
})