const fs = require('fs')

const papa = require('papaparse')

const csvFilePathDeliveries = 'src/data/deliveries.csv'

const csvFilePathMatches = 'src/data/matches.csv'


fs.readFile(csvFilePathDeliveries, 'utf-8', (err, data) => {
    if(err) {
        console.log('Error in reading the file.')
        return
    }
    const parsedData = papa.parse(data, {
        header : true
    })
    const deliveriesData = parsedData.data;

    fs.readFile(csvFilePathMatches, 'utf-8', (err, data) => {
        if(err) {
            console.log('Error in reading the file.')
            return
        }
        const parsedData = papa.parse(data, {
            header : true
        })

        const matchesData = parsedData.data

        function strikerateOfBatsmenEverySeason(matchesData, deliveriesData) {
            const distinctYears = []
            for (let i = 0; i < matchesData.length; i++) {
                const year = matchesData[i]['season']
                if (year !== undefined && distinctYears.indexOf(year) === -1) {
                    distinctYears.push(year)
                }
            }
            console.log(distinctYears)
        
            const matchIdForSeason = {}
            for (let i = 0; i < distinctYears.length; i++) {
                const year = distinctYears[i]
                const yearList = new Set()
                for (let j = 0; j < matchesData.length; j++) {
                    if (matchesData[j]['season'] === year) {
                        yearList.add(matchesData[j]['id'])
                    }
                }
                console.log(yearList)
                matchIdForSeason[year] = yearList
            }
        
            const toatlRunsByBatsmanSeason = {}
            for (let i = 0; i < distinctYears.length; i++) {
                const year = distinctYears[i]
                const result = {}
                for (let j = 0; j < deliveriesData.length; j++) {
                    const delivery = deliveriesData[j]
                    if (matchIdForSeason[year].has(delivery['match_id']) !== -1) {
                        const batsman = delivery['batsman']
                        const runs = parseInt(delivery['batsman_runs'])
                        const wideRuns = delivery['wide_runs']
                        const noballRuns = delivery['noball_runs']
        
                        if (!result[batsman]) {
                            result[batsman] = { runs: 0, balls: 0 }
                        }
        
                        result[batsman].runs += runs
        
                        if (wideRuns === '0' && noballRuns === '0') {
                            result[batsman].balls++
                        }
                    }
                }
                toatlRunsByBatsmanSeason[year] = result
            }
        
            const strikeRateByBatsmanSeason = {}
            for (let i = 0; i < distinctYears.length; i++) {
                const year = distinctYears[i]
                const yearStats = {}
                const batsmenStats = Object.entries(toatlRunsByBatsmanSeason[year]);
                for (let j = 0; j < batsmenStats.length; j++) {
                    const [batsman, stats] = batsmenStats[j]
                    const { runs, balls } = stats
                    yearStats[batsman] = ((runs / balls) * 100).toFixed(2)
                }
                strikeRateByBatsmanSeason[year] = yearStats
            }
        
            return strikeRateByBatsmanSeason;
        }

        
        
        let answer = strikerateOfBatsmenEverySeason(matchesData, deliveriesData)
    
        const jsonData = JSON.stringify(answer)

        fs.writeFile('src/public/output/7-strike-rate-of-batsman-for-each-season.json', jsonData, 'utf8', (err) => {
            if (err) {
            console.error('Error writing JSON data to file:', err)
                return
            }
            console.log('CSV data converted to JSON and saved as output.json')
        })
    })
})