const fs = require('fs')

const papa = require('papaparse')

const csvFilePath = 'src/data/matches.csv'

fs.readFile(csvFilePath, 'utf-8', (err, data) => {
    if(err) {
        console.log('Error in reading the file.')
        return
    }

    const parsedData = papa.parse(data, {
        header : true
    })

    const matchesOfAllYears = parsedData.data;
    
    let winnerOfTossAndGame = (matchesOfAllYears) => {
        let teamsDict = {}
        for(let match of matchesOfAllYears){
            if(match['winner'] === match['toss_winner']){
                if(teamsDict[match['winner']] === undefined){
                    teamsDict[match['winner']] = 1
                }
                else{
                    teamsDict[match['winner']] += 1
                }
            }
        }
        return teamsDict
    }


    let answer = winnerOfTossAndGame(matchesOfAllYears)
    console.log(answer)

    const jsonData = JSON.stringify(answer)

    fs.writeFile('src/public/output/5-each-team-won-toss-won-match.json', jsonData, 'utf8', (err) => {
        if (err) {
          console.error('Error writing JSON data to file:', err)
          return
        }
        console.log('CSV data converted to JSON and saved as output.json')
    })
})