const fs = require('fs')

const papa = require('papaparse')

const csvFilePath = 'src/data/matches.csv'

fs.readFile(csvFilePath, 'utf-8', (err, data) => {
    if(err) {
        console.log('Error in reading the file.')
        return
    }

    const parsedData = papa.parse(data, {
        header : true
    })

    const matchesOfAllYears = parsedData.data
    
    let playerOfTheMatch = (matchesOfAllYears) => {
        let playerDict = {}
        for(let match of matchesOfAllYears){
            let playerOfTheMatch = match['player_of_match']
            let season = match['season']
            if(playerDict[season] === undefined){
                playerDict[season] = {}
                playerDict[season][playerOfTheMatch] = 1
            }
            else{
                if(playerDict[season][playerOfTheMatch] === undefined){
                    playerDict[season][playerOfTheMatch] = 1
                }
                else{
                    playerDict[season][playerOfTheMatch] += 1
                }
            }
        }
        let playerDictUltimate = {}
        for(let season in playerDict){
            let maxAwards = 0;
            let playerOfSeason = '';
            for(let name in playerDict[season]){
                if(playerDict[season][name] > maxAwards){
                    maxAwards = playerDict[season][name]
                    playerOfSeason = name;
                    playerDictUltimate[season]= name
                }
            }
        }
        
        return playerDictUltimate
    }


    let answer = playerOfTheMatch(matchesOfAllYears)
    console.log(answer)

    const jsonData = JSON.stringify(answer)

    fs.writeFile('src/public/output/6-highest-player-of-match-awards-per-season.json', jsonData, 'utf8', (err) => {
        if (err) {
          console.error('Error writing JSON data to file:', err)
          return
        }
        console.log('CSV data converted to JSON and saved as output.json')
      })
})