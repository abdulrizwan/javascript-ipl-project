const fs = require('fs')

const papa = require('papaparse')

const csvFilePath = 'src/data/deliveries.csv'

fs.readFile(csvFilePath, 'utf-8', (err, data) => {
    if(err) {
        console.log('Error in reading the file.')
        return
    }

    const parsedData = papa.parse(data, {
        header : true
    })

    const deliveries = parsedData.data;
    
    let playerDismissedFunc = (deliveries) => {
        let playerDisDict = {}
        let count = 0;
        for(let delivery of deliveries){
            let dismissal_kind = delivery['dismissal_kind']
            if(dismissal_kind){
                if(playerDisDict[delivery['batsman']] === undefined){
                    playerDisDict[delivery['batsman']] = {}
                    if(playerDisDict[delivery['batsman']][delivery['bowler']] === undefined){
                        playerDisDict[delivery['batsman']][delivery['bowler']] = 1
                    }
                    else{
                        playerDisDict[delivery['batsman']][delivery['bowler']] += 1
                    }
                }
                else{
                    if(playerDisDict[delivery['batsman']][delivery['bowler']] === undefined){
                        playerDisDict[delivery['batsman']][delivery['bowler']] = 1
                    }
                    else{
                        playerDisDict[delivery['batsman']][delivery['bowler']] += 1
                    }
                }
            }
        }
        return playerDisDict
    }

    let answer = playerDismissedFunc(deliveries)
    console.log(answer)

    const jsonData = JSON.stringify(answer)

    fs.writeFile('src/public/output/8-highest-no-of-times-dismissed-by-player.json', jsonData, 'utf8', (err) => {
        if (err) {
          console.error('Error writing JSON data to file:', err)
          return
        }
        console.log('CSV data converted to JSON and saved as output.json')
      })
})