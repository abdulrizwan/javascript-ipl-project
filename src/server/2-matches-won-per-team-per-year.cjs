const fs = require('fs')

const papa = require('papaparse')

const csvFilePath = 'src/data/matches.csv'

fs.readFile(csvFilePath, 'utf-8', (err, data) => {
    if(err) {
        console.log('Error in reading the file.')
        return
    }

    const parsedData = papa.parse(data, {
        header : true
    })

    const matchesOfAllYears = parsedData.data;

    const getMatchesPerTeamPerYear = (matchesOfAllYears) => {
        let totalMatches = matchesOfAllYears.reduce((accumulator, match) => {
            let team1 = match['winner']
            let season = match['season']
            console.log(typeof season)
            if(accumulator[team1] === undefined){
                accumulator[team1] = {}
                accumulator[team1][season] = 1
                
            }
            else{
                if(accumulator[team1][season] === undefined){
                    accumulator[team1][season] = 1
                }
                else{
                    accumulator[team1][season] += 1
                }
            }
            
            return accumulator
        }, {})
        return totalMatches
    }

    let answer = getMatchesPerTeamPerYear(matchesOfAllYears)

    console.log(answer)

    const jsonData = JSON.stringify(answer);

    fs.writeFile('src/public/output/2-matches-won-per-team-per-year.json', jsonData, 'utf8', (err) => {
        if (err) {
          console.error('Error writing JSON data to file:', err)
          return
        }
        console.log('CSV data converted to JSON and saved as output.json')
      })
})