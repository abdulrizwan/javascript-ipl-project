const fs = require('fs')

const papa = require('papaparse')

const csvFilePath = 'src/data/matches.csv'

fs.readFile(csvFilePath, 'utf-8', (err, data) => {
    if(err) {
        console.log('Error in reading the file.')
        return
    }

    const parsedData = papa.parse(data, {
        header : true
    })

    const matchesOfAllYears = parsedData.data;

    const getMatchesPerYear = (matchesOfAllYears) => {
        let totalMatches = matchesOfAllYears.reduce((accumulator, match) => {
            if(accumulator[match['season']] === undefined){
                if(match['season'] !== undefined){
                    accumulator[match['season']] = 1
                }
                return accumulator
            }
            else{
                accumulator[match['season']] += 1
                return accumulator
            }
        }, {})
        return totalMatches
    }
    


    let answer = getMatchesPerYear(matchesOfAllYears)

    const jsonData = JSON.stringify(answer);

    fs.writeFile('src/public/output/1-matches-per-year.json', jsonData, 'utf8', (err) => {
        if (err) {
          console.error('Error writing JSON data to file:', err)
          return
        }
        console.log('CSV data converted to JSON and saved as output.json')
      })
})

