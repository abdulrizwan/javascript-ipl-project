const fs = require('fs')

const papa = require('papaparse')

const csvFilePathDeliveries = 'src/data/deliveries.csv'

const csvFilePathMatches = 'src/data/matches.csv'


fs.readFile(csvFilePathDeliveries, 'utf-8', (err, data) => {

    if(err) {
        console.log('Error in reading the file.')
        return
    }
    const parsedData = papa.parse(data, {
        header : true
    })
    const deliveries = parsedData.data;

    fs.readFile(csvFilePathMatches, 'utf-8', (err, data) => {
        if(err) {
            console.log('Error in reading the file.')
            return
        }
        const parsedData = papa.parse(data, {
            header : true
        })

        const matches = parsedData.data;

        const matchIdSet = (matches) => {
            let extraRuns = matches.reduce((accumulator, match) => {
                let season = match['season']
                if(season == '2015'){
                    accumulator.add(match['id'])
                }
                return accumulator
            }, new Set())
            return extraRuns
        }
        
        const economicalBowlers = (deliveries) => {
            let idSet = matchIdSet(matches)
            console.log(idSet)
            let ecoBowlersDict = {}
            for(let delivery of deliveries){
                if(idSet.has(delivery['match_id'])){
                    let bowler = delivery['bowler']
                    if(ecoBowlersDict[bowler] === undefined){
                        ecoBowlersDict[bowler] = [parseInt(delivery['total_runs']), 1]
                    }
                    else{
                        ecoBowlersDict[bowler] = [ecoBowlersDict[bowler][0] + parseInt(delivery['total_runs']), ecoBowlersDict[bowler][1]+1]
                    }
                }
            }
            for(let eachBowler in ecoBowlersDict){
                ecoBowlersDict[eachBowler] = (ecoBowlersDict[eachBowler][0])/(ecoBowlersDict[eachBowler][1])
            }
            
            let result = Object.entries(ecoBowlersDict).sort((a,b) => b[1] - a[1])
            return result.slice(0,11);
        }


        let answer = economicalBowlers(deliveries)
        console.log(answer)

        const jsonData = JSON.stringify(answer)

        fs.writeFile('src/public/output/4-top-10-economical-bowlers.json', jsonData, 'utf8', (err) => {
            if (err) {
            console.error('Error writing JSON data to file:', err)
                return
            }
            console.log('CSV data converted to JSON and saved as output.json')
        })
    })
})