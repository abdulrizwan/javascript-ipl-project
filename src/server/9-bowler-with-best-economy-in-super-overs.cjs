const fs = require('fs')

const papa = require('papaparse')

const csvFilePath = 'src/data/deliveries.csv'

fs.readFile(csvFilePath, 'utf-8', (err, data) => {
    if(err) {
        console.log('Error in reading the file.')
        return
    }

    const parsedData = papa.parse(data, {
        header : true
    })

    const deliveries = parsedData.data;
    
    let economicBowlerForSuperOver = (deliveries) => {
        let economicBowlerDict = {}
        for(let delivery of deliveries){
            if(delivery['is_super_over'] == '1'){
                if(economicBowlerDict[delivery['bowler']] === undefined){
                    economicBowlerDict[delivery['bowler']] = [parseInt(delivery['total_runs']), 1]
                    
                }
                else{
                    economicBowlerDict[delivery['bowler']][0] += parseInt(delivery['total_runs'])
                    economicBowlerDict[delivery['bowler']][1] += 1
                }
            }
        }
        for(let player in economicBowlerDict){
            economicBowlerDict[player] = economicBowlerDict[player][0]/economicBowlerDict[player][1]
        }
        let result = Object.entries(economicBowlerDict).sort((a,b) => b[1] - a[1])
        return result[0][0]
    }

    let answer = economicBowlerForSuperOver(deliveries)

    console.log(answer)

    const jsonData = JSON.stringify(answer)

    fs.writeFile('src/public/output/9-bowler-with-best-economy-in-super-overs.json', jsonData, 'utf8', (err) => {
        if (err) {
          console.error('Error writing JSON data to file:', err)
          return
        }
        console.log('CSV data converted to JSON and saved as output.json')
      })
})